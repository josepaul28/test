//
//  FeedItem+CoreDataClass.swift
//  test
//
//  Created by Paul Soto on 10/01/17.
//  Copyright © 2017 Paul Soto. All rights reserved.
//

import Foundation
import CoreData
import ObjectMapper

@objc(FeedItem)
public class FeedItem: NSManagedObject, Mappable {
    required convenience public init?(map: Map){
        let entity =  NSEntityDescription.entity(forEntityName: "FeedItem",
                                                 in:DataBaseHandler.sharedInstance.managedObjectContext)
        self.init(entity: entity!, insertInto: DataBaseHandler.sharedInstance.managedObjectContext)
    }
    
    public func mapping(map: Map) {
        name <- map["name"]
        image <- map["photo"]
        realName <- map["realName"]
        height <- map["height"]
        abilities <- map["abilities"]
        groups <- map["groups"]
        power <- map["power"]
    }
}
