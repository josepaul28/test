//
//  FeedItem+CoreDataProperties.swift
//  test
//
//  Created by Paul Soto on 10/01/17.
//  Copyright © 2017 Paul Soto. All rights reserved.
//

import Foundation
import CoreData


extension FeedItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FeedItem> {
        return NSFetchRequest<FeedItem>(entityName: "FeedItem");
    }

    @NSManaged public var name: String?
    @NSManaged public var image: String?
    @NSManaged public var realName: String?
    @NSManaged public var height: String?
    @NSManaged public var abilities: String?
    @NSManaged public var groups: String?
    @NSManaged public var power: String?

}
