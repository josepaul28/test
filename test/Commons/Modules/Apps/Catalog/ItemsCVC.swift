//
//  ItemsCVC.swift
//  test
//
//  Created by Paul Soto on 11/01/17.
//  Copyright © 2017 Paul Soto. All rights reserved.
//

import UIKit
import SDWebImage
import ObjectMapper

class ItemsCVC: UICollectionViewController, WebServiceDelegate {
    
    var items: [FeedItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Marvel"
        self.collectionView?.backgroundColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getProducts()
    }
    
    func getProducts() {
        WebService.sharedInstance.delegate = self
        _ = WebService.sharedInstance.excuteRequestWithJsonResponse(
            endpoint: Constants.Api.endpoints.feed,
            parameters:  [:],
            method: .get
        )
    }
    
    //MARK: webservice
    func didFinishRequest(data: AnyObject?, endpoint: String, extraParameters: [String:AnyObject]?) {
        switch endpoint {
        default:
            if let data = data as? [String : AnyObject] {
                let JSON:Map = Map(mappingType: .fromJSON, JSON: data)
                //clear database data
                DataBaseHandler.sharedInstance.deleteAllFrom(entity: "FeedItem")
                self.items.removeAll()
                if let results = Mapper<FeedItem>().mapArray(JSONObject: JSON["superheroes"].currentValue) {
                    self.items.append(contentsOf: results)
                    self.collectionView?.reloadData()
                    DataBaseHandler.sharedInstance.saveContext()
                }
            }
        }
    }
    
    func onErrorRequest(error: Error, endpoint: String) {
        if self.items.isEmpty {
            //show data from DB if exist
            self.items = (DataBaseHandler.sharedInstance.fetch(entity: "FeedItem") as? [FeedItem]) ?? []
            self.collectionView?.reloadData()
        }
    }
    
    //MARK: CollectionView
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AppsCVCell", for: indexPath) as! AppsCVCell
        cell.itemNameLabel.contentMode = .scaleAspectFit
        if let url = self.items[indexPath.row].image {
            cell.itemImageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "no-picture"))
            cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height / 6
            cell.itemImageView.clipsToBounds = true
        } else {
            cell.itemImageView.image = UIImage(named: "no-picture")
        }
        cell.itemNameLabel.text = self.items[indexPath.row].name
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let bounds = collectionView.cellForItem(at: indexPath)?.bounds {
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: [], animations: {
                collectionView.cellForItem(at: indexPath)?.bounds.size = CGSize(width: bounds.size.width - 20, height: bounds.size.height - 20)
            }, completion: { _ in
                let detailVC = DetailVC.init(nibName: "DetailVC", bundle: nil)
                detailVC.view.frame = self.view.bounds
                detailVC.modalPresentationStyle = .overCurrentContext
                detailVC.item = self.items[indexPath.row]
                self.present(detailVC, animated: false, completion: {
                    collectionView.cellForItem(at: indexPath)?.bounds.size = bounds.size
                })
            })
        }
    }
}
