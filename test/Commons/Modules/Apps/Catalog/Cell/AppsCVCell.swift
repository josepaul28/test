//
//  AppsCVCell.swift
//  test
//
//  Created by Paul Soto on 11/01/17.
//  Copyright © 2017 Paul Soto. All rights reserved.
//

import UIKit

class AppsCVCell: UICollectionViewCell {
    
    
    @IBOutlet weak var itemImageView: UIImageView!
    
    @IBOutlet weak var itemNameLabel: UILabel!
    
}
