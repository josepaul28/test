//
//  DetailVC.swift
//  test
//
//  Created by Paul Soto on 12/01/17.
//  Copyright © 2017 Paul Soto. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    @IBOutlet weak var infoWrapperView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var realNameLabel: UILabel!
    @IBOutlet weak var moreDetailsTextView: UITextView!
    var item: FeedItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.subviews.first?.alpha = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 1) {
            self.view.subviews.first?.alpha = 1
        }
        if let item = self.item {
            if let url = item.image {
                self.itemImageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "no-picture"))
                self.itemImageView.layer.cornerRadius = self.itemImageView.frame.size.height / 6
                self.itemImageView.clipsToBounds = true
            } else {
                self.itemImageView.image = UIImage(named: "no-picture")
            }
            self.itemNameLabel.text = item.name
            self.realNameLabel.text = item.realName
            
            self.moreDetailsTextView.text.removeAll()
            self.moreDetailsTextView.text =
                "Height: " + (item.height ?? "") + "\n\n" +
                "Power: " + (item.power ?? "") + "\n\n" +
                "Abilities: " + (item.abilities ?? "") + "\n\n" +
                "Groups: " + (item.groups ?? "")
            
            self.infoWrapperView.layer.cornerRadius = 10
            self.infoWrapperView.clipsToBounds = true
            self.infoWrapperView.isHidden = false
        }
    }
    
    @IBAction func lookAtGoogleAction(_ sender: Any) {
        
        if let name = self.item?.name?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed), let url = URL(string: "http://www.google.com/search?q=" + name) {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
