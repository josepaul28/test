//
//  WebService.swift
//  shop
//
//  Created by Paul Jose Soto Vasallo on 20/05/16.
//  Copyright © 2016 Paul Jose Soto Vasallo. All rights reserved.
//

import Foundation
import Alamofire

protocol WebServiceDelegate: class {
    func didFinishRequest(data: AnyObject?, endpoint: String, extraParameters: [String: AnyObject]?)
    func onErrorRequest(error: Error, endpoint: String)
}

class WebService {
    
    //Singleton WebService
    static let sharedInstance = WebService()
    
    private let net:NetworkReachabilityManager
    
    weak var delegate:WebServiceDelegate?
    
    var noInternetlabel:UILabel?
    
    private init(){
        if let applicationDelegate:AppDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let window:UIWindow = applicationDelegate.window {
                self.noInternetlabel = UILabel(frame: CGRect(x: 0, y: 0, width: window.bounds.size.width, height: 64))
                self.noInternetlabel?.backgroundColor = UIColor.red
                self.noInternetlabel?.text = "No internet conection"
                self.noInternetlabel?.textAlignment = .center
                self.noInternetlabel?.textColor = UIColor.white
            }
        }
        self.net = NetworkReachabilityManager()!
        self.net.startListening()
        self.net.listener = { status in
            self.checkInternet()
        }
    }
    
    func checkInternet() {
        if  self.net.isReachable {
            noInternetlabel?.removeFromSuperview()
        } else {
            if let applicationDelegate:AppDelegate = UIApplication.shared.delegate as? AppDelegate {
                if let window:UIWindow = applicationDelegate.window, let noInternetlabel = self.noInternetlabel {
                    window.addSubview(noInternetlabel)
                }
            }
        }
    }
    
    func excuteRequestWithJsonResponse(endpoint: String, parameters: [String: AnyObject] = [:], method: Alamofire.HTTPMethod = .get, extraParameters: [String: AnyObject]? = nil) -> Request? {
        if let delegate = self.delegate {
            let request = Alamofire.request(Constants.Api.baseUrl + endpoint, method: method, parameters: parameters)
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        delegate.didFinishRequest(data: response.result.value as AnyObject?, endpoint: endpoint, extraParameters: extraParameters)
                    case .failure(let error):
                        print(error)
                        let deadline = DispatchTime.now() + .seconds(10)
                        DispatchQueue.main.asyncAfter(deadline: deadline) {
                            print("Retrying")
                            _ = self.excuteRequestWithJsonResponse(endpoint: endpoint, parameters: parameters, method: method, extraParameters: extraParameters)
                        }
                        delegate.onErrorRequest(error: error, endpoint: endpoint)
                    }
            }
            print(request)
            return request
        } else {
            print("Delegate not set!")
            return nil
        }
    }
    
    func cancellRequest(request: AnyObject?) {
        if let request = request as? Request {
            request.cancel()
        }
    }
}
