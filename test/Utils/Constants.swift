//
//  Constants.swift
//  Feed
//
//  Created by Paul Soto on 30/11/16.
//  Copyright © 2016 Paul Soto. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct Configs {
        static let currency = "$"
    }
    
    struct Api {
        
        static let baseUrl = "https://api.myjson.com/"
        
        struct endpoints {
            static let feed = "bins/bvyob"
        }
    }
    
    struct Colors {
        static let appMainContrastColor = UIColor.white
    }
}
